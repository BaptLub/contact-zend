<?php

return array(//TABLEAU DES PARAMETRES
    'router' => array(//ROUTAGE
        'routes' => array(//enssemble des routes
            'rContact' => array(//déclaration d'une route nommé 'rContact'
                'type' => 'Zend\Mvc\Router\Http\Literal', //type de route
                'options' => array(
                    'route' => '/', //url de la route
                    'defaults' => array(
                        //controleur initialisé par défaut
                        'controller' => 'CtrlIndex',
                        //action par défaut [méthode exécutée par le controleur ]
                        'action' => 'index', //[méthode -indexAction- ]
                    ),
                ),
            ),
            'rContact-Gestion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/:action[/:id]',
                    'defaults' => array(
                        'controller' => 'CtrlIndex',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                        ),
                    ),
                ),
            ),
        ),
    ), //Fin ROUTAGE
    'controllers' => array(//CONTROLLER
        'invokables' => array(//déclaration des controlleurs dynamiquement initialisables
            'CtrlIndex' => 'Contact\Controller\IndexController'
        ),
    ), //FIN CONTROLLER
    'view_manager' => array(//GESTIONNAIRE DE VUE
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'contact/index/index' => __DIR__ . '/../view/contact/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ), //FIN GESTIONNAIRE DE VUE
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
); //FIN TABLEAU DES PARAMETRES